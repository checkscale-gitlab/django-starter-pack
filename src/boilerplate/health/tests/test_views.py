class TestSystemHealth:

    def test_returns_200_with_ok_status(self, client):
        res = client.get('/health/system/')

        assert res.status_code == 200
