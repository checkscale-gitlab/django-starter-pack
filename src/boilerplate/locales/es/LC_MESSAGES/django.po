# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-12-26 16:03+0000\n"
"PO-Revision-Date: 2021-12-26 16:04+0000\n"
"Last-Translator:   <>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Translated-Using: django-rosetta 0.9.8\n"

#: boilerplate/settings/django.py:130
msgid "English"
msgstr "Inglés"

#: boilerplate/settings/django.py:131
msgid "Spanish"
msgstr "Español"

#: boilerplate/templates/base.html:23
msgid "The Everyday Django Boilerplate"
msgstr "El Boilerplate de Django de todos los días"

#: boilerplate/templates/base.html:32
msgid ""
"A quick and easy to use Django Boilerplate ready for production in 10 min. "
"No bloat guaranteed."
msgstr ""
"Un Django Boilerplate rápido y fácil de usar listo para la producción en 10 "
"min. Sin tonterías garantizada."

#: boilerplate/templates/base.html:37
msgid "Documentation"
msgstr "Documentación"

#: boilerplate/templates/base.html:44
msgid "Rosetta (Local Only)"
msgstr "Rosetta (Solo Local)"

#: boilerplate/templates/base.html:47
msgid "Contact Andrew"
msgstr "Contactar a Andrew"
