#!/usr/bin/env bash
set -ex
pytest --cov-report term-missing:skip-covered --cov-report xml:coverage.xml --cov .
