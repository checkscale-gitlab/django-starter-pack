pytest~=6.2.5
pytest-cov
pytest-django
pytest-mock
pytest-sugar
pytest-responses
flake8-isort
freezegun
