from .base import *  # noqa

INSTALLED_APPS += [
    'debug_toolbar',
    'rosetta',
]
DEBUG = True
LOGOUT_REDIRECT_URL = '/'
ROSETTA_LOGIN_URL = '/admin/login'
MIDDLEWARE = [
    'debug_toolbar.middleware.DebugToolbarMiddleware'
] + MIDDLEWARE

def show_toolbar(request):
    return True

DEBUG_TOOLBAR_CONFIG = {
    "SHOW_TOOLBAR_CALLBACK": show_toolbar,
}
