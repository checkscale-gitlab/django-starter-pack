#!/usr/bin/env bash

exec gunicorn boilerplate.wsgi:application \
  --config /var/gunicorn/gunicorn.py \
  "$@"
